package org.erouvas.sftp.container;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.erouvas.sftp.utils.RefVars;
import org.apache.camel.FluentProducerTemplate;


@Path("/tryme")
public class EntryPointResource {

    @Inject
    FluentProducerTemplate producerTemplate;

    @GET
    @Path("/sftp")
    public String sftpCall() {
        
        String response = producerTemplate
                .to(RefVars.ROUTE_START).request(String.class);

        return response;
    }
}