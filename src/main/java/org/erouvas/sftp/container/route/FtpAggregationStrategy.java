package org.erouvas.sftp.container.route;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpAggregationStrategy implements AggregationStrategy {

    private static final Logger log = LoggerFactory.getLogger(FtpAggregationStrategy.class);

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        if (oldExchange == null) {
            return newExchange;
        }

        if (newExchange != null && newExchange.getIn() != null) {

            String newBody = newExchange.getIn().getBody(String.class);
            oldExchange.getIn().setBody(newBody);
        }

        return oldExchange;
    }
}
