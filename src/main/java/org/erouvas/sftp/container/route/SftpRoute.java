package org.erouvas.sftp.container.route;

import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.builder.RouteBuilder;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.erouvas.sftp.utils.RefVars;

@ApplicationScoped
public class SftpRoute extends RouteBuilder {

    @ConfigProperty(name = "erouvas.sftp.user")
    String sftpUser;

    @ConfigProperty(name = "erouvas.sftp.passwd")
    String sftpPasswd;

    @ConfigProperty(name = "erouvas.sftp.server")
    String sftpServer;

    @ConfigProperty(name = "erouvas.sftp.port")
    String sftpPort;

    @ConfigProperty(name = "erouvas.sftp.input.directory")
    String sftpInputDir;

    @ConfigProperty(name = "erouvas.sftp.inputFile")
    String sftpInputFile;

    @ConfigProperty(name = "erouvas.sftp.output.directory")
    String sftpOutputDir;

    @ConfigProperty(name = "erouvas.sftp.privateKeyFile")
    String sftpPrivateKey;

    @Override
    public void configure() throws Exception {
        // --------------------------------------------------------------------
        // --
        // -- CONFIGURE AND SETUP ROUTE - START
        // --
        // -------------------------------------------------------------------
        from(RefVars.ROUTE_START)
                .setHeader(RefVars.SFTP_USER, simple(sftpUser))
                .setHeader(RefVars.SFTP_PASSWD, simple(sftpPasswd))
                .setHeader(RefVars.SFTP_SERVER, simple(sftpServer))
                .setHeader(RefVars.SFTP_PORT, simple(sftpPort))
                .setHeader(RefVars.SFTP_INPUT_DIR, simple(sftpInputDir))
                .setHeader(RefVars.SFTP_INPUT_FILE, simple(sftpInputFile))
                .setHeader(RefVars.SFTP_OUTPUT_DIR, simple(sftpOutputDir))
                .setHeader(RefVars.SFTP_PRIVATE_KEY_FILE, simple(sftpPrivateKey))
                .process(new FtpInCheckProcessor())
                .log("SFTP_TARGET_URI:${header." + RefVars.SFTP_INGEST_URI + "}")
                .pollEnrich().simple("${header." + RefVars.SFTP_INGEST_URI + "}").timeout(30).aggregationStrategy(new FtpAggregationStrategy())
                .log("SFTP_RESPONSE:[${body}]")
                .end();
        // --------------------------------------------------------------------
        // --
        // -- CONFIGURE AND SETUP ROUTE - END
        // --
        // --------------------------------------------------------------------
    }

}
