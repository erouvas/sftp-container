package org.erouvas.sftp.container.route;

import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.erouvas.sftp.utils.RefVars;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpInCheckProcessor implements Processor {

    private static final Logger log = LoggerFactory.getLogger(FtpInCheckProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        Map<String, Object> inHeaders = exchange.getIn().getHeaders();

        String ftpServer = (String) inHeaders.get(RefVars.SFTP_SERVER);
        String ftpPort = (String) inHeaders.get(RefVars.SFTP_PORT);
        String ftpInDir = (String) inHeaders.get(RefVars.SFTP_INPUT_DIR);
        String ftpInFile = (String) inHeaders.get(RefVars.SFTP_INPUT_FILE);
        String ftpOutDir = (String) inHeaders.get(RefVars.SFTP_OUTPUT_DIR);
        String ftpUser = (String) inHeaders.get(RefVars.SFTP_USER);
        String ftpPasswd = (String) inHeaders.get(RefVars.SFTP_PASSWD);
        String sftpPrivateKey = (String) inHeaders.get(RefVars.SFTP_PRIVATE_KEY_FILE);

        String ftpProtocol = "sftp";
        StringBuilder ftpUri = new StringBuilder(ftpProtocol).append("://");
        ftpUri.append(ftpUser).append('@');
        ftpUri
                .append(ftpServer)
                .append(':').append(ftpPort)
                .append("/").append(ftpInDir);
        log.info("FTP_SERVER WILL USE [ {} ]", ftpUri.toString());
        log.info("FTP_SERVER WILL USE PRIVATE_KEY [ {} ]", sftpPrivateKey);
        StringBuilder ftpPerFile = new StringBuilder(ftpUri.toString());
        ftpPerFile
                .append("?privateKeyFile=").append(sftpPrivateKey)
                .append("&binary=true")
                .append("&disconnect=true&autoCreate=false&fileName=").append(ftpInFile)
                .append("&throwExceptionOnConnectFailed=true")
                .append("&ignoreFileNotFoundOrPermissionError=false");
        //

        inHeaders.put(RefVars.SFTP_INGEST_URI, ftpPerFile);

        exchange.getMessage().setHeaders(inHeaders);
    }

}
