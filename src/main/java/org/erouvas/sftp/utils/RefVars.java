/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.erouvas.sftp.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RefVars {

    private static final Logger log = LoggerFactory.getLogger(RefVars.class);

    public static final String SFTP_USER = "sftpUser";
    public static final String SFTP_PASSWD = "sftpPasswd";
    public static final String SFTP_SERVER = "sftpServer";
    public static final String SFTP_PORT = "sftpPort";
    public static final String SFTP_URI = "sftpUri";
    public static final String SFTP_INPUT_DIR = "SFTP_INPUT_DIR";
    public static final String SFTP_INPUT_FILE = "SFTP_INPUT_FILE";
    public static final String SFTP_OUTPUT_DIR = "sftpOutputDir";
    public static final String SFTP_OUTPUT_URI = "sFTP_OUTPUT_URI";
    public static final String SFTP_PRIVATE_KEY_FILE = "SFTP_PRIVATE_KEY_FILE";
    //
    public static final String ROUTE_START = "direct:start-me-up";
    public static final String SFTP_INGEST_URI = "SFTP_INGEST_URI";

}
