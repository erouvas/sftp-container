package org.erouvas.sftp.container;


import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.util.logging.Logger;
import static org.hamcrest.CoreMatchers.containsString;
import org.junit.ClassRule;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.FixedHostPortGenericContainer;


@QuarkusTest
public class EntryPointResourceTest {

    private static final Logger LOG = Logger.getLogger(EntryPointResourceTest.class.getName());

    @ClassRule
    public static FixedHostPortGenericContainer sftpContainer
            = new FixedHostPortGenericContainer<>("atmoz/sftp:latest")
                    .withFixedExposedPort(2222, 22).withEnv("SFTP_USERS", "orkadian:grimbister:1001:100:input-data,result-data")
                    .withClasspathResourceMapping("sftp-keys/unit-test_rsa.pub", "/home/orkadian/.ssh/keys/id_rsa.pub", BindMode.READ_ONLY)
                    .withClasspathResourceMapping("test-data/input.txt", "/home/orkadian/input-data/input.txt", BindMode.READ_ONLY);

    @Test
    public void testHelloEndpoint() {
        LOG.info("WAITING FOR CONTAINER");
        sftpContainer.start();
        LOG.info("SFTP_CONTAINER STARTED");

        LOG.info("CONTINUING WITH " + sftpContainer.getHost() + ":" + sftpContainer.getMappedPort(22));
        given()
                .when().get("/tryme/sftp")
                .then()
                .statusCode(200)
                .body(containsString("SFTP"));
        sftpContainer.stop();
    }
}