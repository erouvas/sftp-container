package org.erouvas.sftp.container;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeEntryPointResourceIT extends EntryPointResourceTest {

    // Execute the same tests but in native mode.
}